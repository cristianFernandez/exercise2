﻿using System;
using System.Collections.Generic;

namespace Exercise2 {
    class Program {
        public static String HELLO_MSG = "ROVER_VALIDATOR - AUTHOR: CRISTIAN FERNÁNDEZ DEL POZO - 2018 - 05 -20";
        public static String INST_MSG = "Exercise2 <width> <height> <commands> OR Exercise2 <width> <height> <init_x> <init_y> <Orientation> <commands>";

        static void Main (string[] args) {
            Console.WriteLine (HELLO_MSG);

            if (args.Length != 3 && args.Length != 6) {
                Console.WriteLine (INST_MSG);
                return;
            }

            string instr = "";
            int width = 2;
            int height = 2;
            int x = 0;
            int y = 0;
            Orientation initOrientation = Orientation.N;

            width = Int32.Parse (args[0]);
            height = Int32.Parse (args[1]);

            if (args.Length == 3) {
                instr = args[2];
            } else {
                x = Int32.Parse (args[2]);
                y = Int32.Parse (args[3]);
                initOrientation = ParseOrientation (args[4]);
                instr = args[5];
            }

            Map map4rover = new Map (width, height);
            Rover marsRover = new Rover (map4rover, x, y, initOrientation);

            var result = marsRover.ProcessInstructions (ParseDirections (instr));

            string result2console = string.Format ("{0},{1}", result.ToString (), marsRover.ToString ());

            Console.WriteLine (result2console);

        }

        private static Orientation ParseOrientation (string v) {
            v = v.Trim ().ToUpper ();

            switch (v) {
                case "N":
                    return Orientation.N;

                case "S":
                    return Orientation.S;

                case "E":
                    return Orientation.E;

                case "W":
                    return Orientation.W;

                default:
                    throw new Exception("Orientation is not valid");
            }
        }

        public static List<Direction> ParseDirections (string msg) {
            List<Direction> result = new List<Direction> ();

            msg = msg.Trim ().ToUpper ();

            foreach (var char2pars in msg) {
                result.Add (ParseCharToDir (char2pars));
            }

            return result;
        }

        public static Direction ParseCharToDir (char c) {
            switch (c) {
                case 'A':
                    return Direction.A;

                case 'L':
                    return Direction.L;

                case 'R':
                    return Direction.R;

                default:
                    throw new Exception ("Direction not recognizable: " + c.ToString ());
            }
        }

    }
}