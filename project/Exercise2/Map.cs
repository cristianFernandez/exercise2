﻿

using System;

namespace Exercise2
{
    public class Map
    {
        public int Width { get; }
        public int Height { get; }

        
        public Map(int width, int height)
        {
            if (width <0 || height < 0)
            {
                throw new Exception("The Map bound must be in abs values");
            }
            
            Width = width;
            Height = height;
        }

        public bool IsAValidPosition(int x, int y)
        {
            //If new position is less than 0, error (bottom and left edge). If new position it's outbound (top and right edge), error. 
            return ( (x >= 0 && x < Width) && (y >= 0 && y < Height));

        }
        

        
    }
}