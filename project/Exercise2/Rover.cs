﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters;

namespace Exercise2
{
    public class Rover
    {
        private Map _map;
        
        private int _X = 0;
        private int _Y = 0;
        public Orientation RoverOrientation { get; private set; }
        

        public Rover(Map map, int x = 0, int y = 0, Orientation innitOrientation = Orientation.N)
        {
            _map = map;

            if (!map.IsAValidPosition(x, y))
            {
                throw new Exception("Rover out of bounds");
            }

            _X = x;
            _Y = y;
            RoverOrientation = innitOrientation;
        }

        public bool ProcessInstructions(List<Direction> instructions)
        {
            foreach (var dir in instructions)
            {
                if (!DoAMovement(dir))
                {
                    return false;
                }
            }

            return true;

        }
        
        public bool DoAMovement(Direction dir)
        {
            var newPosition = _getNewPosition(RoverOrientation, dir, out var newOrientation);
            var x = newPosition[0];
            var y = newPosition[1];
            
            
            if (!_map.IsAValidPosition(x, y))
            {
                return false;
            }
            
            _X = x;
            _Y = y;
            RoverOrientation = newOrientation;

            return true;

        }
        
        private int[] _getNewPosition(Orientation or, Direction dir, out Orientation newOrientation)
        {
            int newX = -1;
            int newY = -1;

            newOrientation = or;
    
            //Based on the orientation, we calc next position
            switch (or)
            {
                case Orientation.N:

                    switch (dir)
                    {
                        case Direction.A:
                            newX = _X;
                            newY = _Y+1;
                            break;
                        
                        case Direction.L:
                            newX = _X-1;
                            newY = _Y;
                            newOrientation = Orientation.W;
                            break;
                        
                        case Direction.R:
                            newX = _X+1;
                            newY = _Y;
                            newOrientation = Orientation.E;
                            break;
                    }
                    
                    break;
                    
                case Orientation.S:
                    switch (dir)
                    {
                        case Direction.A:
                            newX = _X;
                            newY = _Y-1;
                            break;
                        
                        case Direction.L:
                            newX = _X+1;
                            newY = _Y;
                            newOrientation = Orientation.E;
                            break;
                        
                        case Direction.R:
                            newX = _X-1;
                            newY = _Y;
                            newOrientation = Orientation.W;
                            break;
                    }
                    
                    break;
                
                case Orientation.E:
                    switch (dir)
                    {
                        case Direction.A:
                            newX = _X+1;
                            newY = _Y;
                            break;
                        
                        case Direction.L:
                            newX = _X;
                            newY = _Y+1;
                            newOrientation = Orientation.N;
                            break;
                        
                        case Direction.R:
                            newX = _X;
                            newY = _Y-1;
                            newOrientation = Orientation.S;
                            break;
                    }
                    
                    break;
                
                case Orientation.W:
                    switch (dir)
                    {
                        case Direction.A:
                            newX = _X-1;
                            newY = _Y;
                            break;
                        
                        case Direction.L:
                            newX = _X;
                            newY = _Y-1;
                            newOrientation = Orientation.S;
                            break;
                        
                        case Direction.R:
                            newX = _X;
                            newY = _Y+1;
                            newOrientation = Orientation.N;
                            break;
                    }
                    
                    break;
            }

            return new int[]{newX,newY};
        }

        public override string ToString()
        {
            return string.Format("{0},({1},{2})",ParseOrientationToSring(RoverOrientation),_X.ToString(),_Y.ToString());
        }

        public static string ParseOrientationToSring(Orientation o)
        {
            switch (o)
            {
                case Orientation.N:
                    return "N";
                case Orientation.S:
                    return "S";
                case Orientation.E:
                    return "E";
                case Orientation.W:
                    return "W";
            }

            return "";
        }
    }
}